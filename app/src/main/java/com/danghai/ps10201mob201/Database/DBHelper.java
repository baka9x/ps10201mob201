package com.danghai.ps10201mob201.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    final public static String DBNAME = "ASSIGNMENT_MOB201";
    final public static int DBVERSION = 1;

    public DBHelper(Context context) {
        super(context, DBNAME, null, DBVERSION);
    }


    private final static String CREATE_COURSES = "CREATE TABLE IF NOT EXISTS COURSES(" +
            "COURSE_CODE VARCHAR NOT NULL PRIMARY KEY," +
            "COURSE_NAME VARCHAR NOT NULL," +
            "DESCRIPTION VARCHAR NOT NULL," +
            "DATE_START VARCHAR NOT NULL," +
            "DATE_END VARCHAR NOT NULL)";
    private final static String CREATE_STUDENTS = "CREATE TABLE IF NOT EXISTS STUDENTS(" +
            "STUDENT_CODE VARCHAR NOT NULL PRIMARY KEY," +
            "STUDENT_NAME VARCHAR NOT NULL," +
            "STUDENT_COURSE VARCHAR NOT NULL," +
            "FOREIGN KEY (STUDENT_COURSE) REFERENCES COURSES(COURSE_CODE))";
    //test schedule

    private final static String CREATE_TESTS_SCHEDULE = "CREATE TABLE IF NOT EXISTS TESTS_SCHEDULE(" +
            "TEST_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
            "TEST_NAME VARCHAR NOT NULL," +
            "TEST_COURSE VARCHAR NOT NULL," +
            "TEST_STUDENT VARCHAR NOT NULL," +
            "FOREIGN KEY (TEST_COURSE) REFERENCES COURSES(COURSE_CODE)," +
            "FOREIGN KEY (TEST_STUDENT) REFERENCES STUDENTS(STUDENT_CODE))";

    private final static String DROP_COURSES = "DROP TABLE IF EXISTS COURSES";
    private final static String DROP_STUDENTS = "DROP TABLE IF EXISTS STUDENTS";
    private final static String DROP_TESTS_SCHEDULE = "DROP TABLE IF EXISTS TESTS_SCHEDULE";

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_COURSES);
        sqLiteDatabase.execSQL(CREATE_STUDENTS);
        sqLiteDatabase.execSQL(CREATE_TESTS_SCHEDULE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(DROP_COURSES);
        sqLiteDatabase.execSQL(DROP_STUDENTS);
        sqLiteDatabase.execSQL(DROP_TESTS_SCHEDULE);
    }
}
