package com.danghai.ps10201mob201.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.ps10201mob201.Model.News;
import com.danghai.ps10201mob201.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder> {
    private Context mContext;
    private List<News> mList;

    public NewsAdapter(Context mContext, List<News> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public NewsAdapter.NewsHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_news, parent, false);

        return new NewsAdapter.NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.NewsHolder holder, int position) {
        News news = mList.get(position);

        holder.title.setText(news.getTitle());
        holder.date.setText(news.getDate());

        Picasso.get().load(news.getThumbnail()).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class NewsHolder extends RecyclerView.ViewHolder {
        ImageView thumbnail;
        TextView title, date;


        public NewsHolder(@NonNull View itemView) {
            super(itemView);
            thumbnail = itemView.findViewById(R.id.iv_thumbnail);
            title = itemView.findViewById(R.id.tv_title);
            date = itemView.findViewById(R.id.tv_date);
        }
    }
}
