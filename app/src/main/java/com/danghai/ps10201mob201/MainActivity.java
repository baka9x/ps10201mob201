package com.danghai.ps10201mob201;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.danghai.ps10201mob201.Fragment.CoursesFragment;
import com.danghai.ps10201mob201.Fragment.MapsFragment;
import com.danghai.ps10201mob201.Fragment.NewsFragment;
import com.danghai.ps10201mob201.Fragment.SocialFragment;
import com.danghai.ps10201mob201.Fragment.TestScheduleFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    Fragment selectFragment = null;
                    switch (menuItem.getItemId()) {
                        case R.id.nav_courses:
                            getSupportActionBar().setTitle("Khóa học");
                            selectFragment = new CoursesFragment();
                            break;
                        case R.id.nav_test_schedule:
                            getSupportActionBar().setTitle("Lịch thi");
                            selectFragment = new TestScheduleFragment();
                            break;
                        case R.id.nav_maps:
                            getSupportActionBar().setTitle("Bản đồ");
                            selectFragment = new MapsFragment();
                            break;
                        case R.id.nav_news:
                            getSupportActionBar().setTitle("Tin tức");
                            selectFragment = new NewsFragment();
                            break;
                        case R.id.nav_social:
                            getSupportActionBar().setTitle("Mạng xã hội");
                            selectFragment = new SocialFragment();

                            break;

                    }
                    getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, selectFragment).commit();

                    return true;

                }
            };

    private void init() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        bottomNav = findViewById(R.id.nav_view);
        bottomNav.setOnNavigationItemSelectedListener(navListener);


    }


}
