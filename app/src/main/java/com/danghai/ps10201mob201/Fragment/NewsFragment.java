package com.danghai.ps10201mob201.Fragment;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.danghai.ps10201mob201.Adapter.NewsAdapter;
import com.danghai.ps10201mob201.MainActivity;
import com.danghai.ps10201mob201.Model.News;
import com.danghai.ps10201mob201.R;
import com.danghai.ps10201mob201.Utils.NewsReader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;


public class NewsFragment extends Fragment {
    NewsAdapter newsAdapter;

   List<News> mNews;

    RecyclerView mListNews;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_news, container, false);
        mListNews = view.findViewById(R.id.rv_list_news);
        mListNews.setHasFixedSize(true);
        mListNews.setLayoutManager(new LinearLayoutManager(getContext()));
        mNews = new ArrayList<>();

        NewsSeedAsyncTask newsSeedAsyncTask = new NewsSeedAsyncTask();
        newsSeedAsyncTask.execute();

        return view;
    }

    @SuppressLint("StaticFieldLeak")
    class NewsSeedAsyncTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL("https://vnexpress.net/rss/giao-duc.rss");
                HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
                InputStream inputStream = httpURLConnection.getInputStream();
                List newses = NewsReader.listNews(inputStream);


                newsAdapter = new NewsAdapter(getContext(), newses);

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListNews.setAdapter(newsAdapter);
                        newsAdapter.notifyDataSetChanged();
                    }
                });

            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
