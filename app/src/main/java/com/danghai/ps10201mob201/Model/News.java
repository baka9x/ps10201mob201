package com.danghai.ps10201mob201.Model;

public class News {

    private String thumbnail;
    private String title;
    private String date;
    private String link;

    public News() {
    }

    public News(String thumbnail, String title, String date, String link) {
        this.thumbnail = thumbnail;
        this.title = title;
        this.date = date;
        this.link = link;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
