package com.danghai.ps10201mob201.Model;

public class Student {

    private String studentCode;
    private String studentName;
    private String studentCourse;

    public Student() {
    }

    public Student(String studentCode, String studentName, String studentCourse) {
        this.studentCode = studentCode;
        this.studentName = studentName;
        this.studentCourse = studentCourse;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentCourse() {
        return studentCourse;
    }

    public void setStudentCourse(String studentCourse) {
        this.studentCourse = studentCourse;
    }
}
