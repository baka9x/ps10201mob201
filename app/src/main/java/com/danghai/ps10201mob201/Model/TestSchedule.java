package com.danghai.ps10201mob201.Model;

public class TestSchedule {
    private int testId;
    private String testName;
    private String testCourse;
    private String testStudent;

    public TestSchedule() {
    }

    public TestSchedule(int testId, String testName, String testCourse, String testStudent) {
        this.testId = testId;
        this.testName = testName;
        this.testCourse = testCourse;
        this.testStudent = testStudent;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getTestCourse() {
        return testCourse;
    }

    public void setTestCourse(String testCourse) {
        this.testCourse = testCourse;
    }

    public String getTestStudent() {
        return testStudent;
    }

    public void setTestStudent(String testStudent) {
        this.testStudent = testStudent;
    }
}
