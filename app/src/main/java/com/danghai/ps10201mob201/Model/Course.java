package com.danghai.ps10201mob201.Model;

public class Course {
    private String courseCode;
    private String courseName;
    private String description;
    private String dateStart;
    private String dateEnd;

    public Course() {
    }

    public Course(String courseCode, String courseName, String description, String dateStart, String dateEnd) {
        this.courseCode = courseCode;
        this.courseName = courseName;
        this.description = description;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }
}
